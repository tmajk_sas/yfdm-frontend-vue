# yfdmfront

> A Vue.js project   

> Author Tomasz Majk - sas.it.tomasz.majk@gmail.com

## Info
Early development version.   
Backend django repo will be publin in few days.   
In config.js mockApi is set to true, so app will be working in mocked mode, no data presistance ;)   
    
The purpose for app is to organize test data in order *ENV/APP/TestDataClass/DataRow*   
Backend will have friendy API to receive test data (for test automation). Usage of API will be explained in Frontend Docs section   

 [Working demo][id]  

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

[id]: http://vps362122.ovh.net/  "YFDM FRONT"
