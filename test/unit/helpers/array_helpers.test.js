import { findIndexById } from '../../../src/helpers/array_helpers'

it('should find index', () => {
    const arr = [
        {id: 2, name: 'lol'},
        {id: 1, name: 'lol'},
        {id: 3, name: 'lol'}
    ]
    expect(findIndexById(arr, 1))
    .toBe(1)
})
