import { getTestApiHelloData } from '../../../../src/api/test_call_api'
import { helloData, dataNotFound, dataError } from './fakeApiData'
describe('demo test 2', () => {
  it('1 should not equal 2', () => {
    expect(1 === 2)
      .toBe(false)
  })
})

it('api test success', () => {
  let fake = false
  let data = helloData
  let fakeFetch = () => {
    fake = true
    return Promise.resolve(data)
  }
  return getTestApiHelloData('sas', fakeFetch)
    .then(response => {
      expect(fake).toBe(true)
      expect(response.username).toBe('sas')
    })
})

it('api test - not found', () => {
  let fake = false
  let data = dataNotFound
  let fakeFetch = () => {
    fake = true
    return Promise.resolve(data)
  }
  return getTestApiHelloData('sas', fakeFetch)
    .then(response => {
      expect(fake).toBe(true)
      expect(response.error.error).toBe('NOT_FOUND')
    })
})

it('api test - other error found', () => {
  let fake = false
  let data = dataError
  let fakeFetch = () => {
    fake = true
    return Promise.resolve(data)
  }
  return getTestApiHelloData('sas', fakeFetch)
    .then(response => {
      expect(fake).toBe(true)
      expect(response.error.error).toBe(dataError.response.error)
    })
})
