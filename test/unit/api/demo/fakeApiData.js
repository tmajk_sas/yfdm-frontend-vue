export const helloData = {
  status: 200,
  data: {
    msg: 'Hello sas',
    status: 'success'
  }
}
export const dataNotFound = {
  status: 404
}

export const dataError = {
  response: {
    status: 506,
    error: '500+_ERROR',
    statusText: 'Inny error 500+'
  }
}
