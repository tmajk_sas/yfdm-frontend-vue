import { appVersion } from '../../config'
// STATE
const state = {
  chekingToken: false,
  globalLoading: false,
  appVersion: appVersion,
  loadingModules: []
}

// GETTERS
const getters = {
  chekingToken (state) {
    return state.chekingToken
  },
  globalLoading (state) {
    return state.globalLoading
  },
  appLoading (state) {
    return state.loadingModules.length > 0 || state.chekingToken
  },
  appVersion (state) {
    return state.appVersion
  }
}

// MUTATIONS
const mutations = {
  startGlobalLoading (state, payload) {
    state.loadingModules.push(payload)
  },
  stopGlobalLoading (state, payload) {
    state.loadingModules.splice(state.loadingModules.indexOf(payload), 1)
  },
  startTokenCheck (state) {
    state.chekingToken = true
  },
  stopTokenCheck (state) {
    state.chekingToken = false
  }
}

// ACTIONS
const actions = {
  startGlobalLoading ({ commit, state }, payload) {
    commit('startGlobalLoading', payload)
  },
  stopGlobalLoading ({ commit, state }, payload) {
    commit('stopGlobalLoading', payload)
  },
  startTokenCheck ({ commit, state }) {
    commit('startTokenCheck')
  },
  stopTokenCheck ({ commit, state }) {
    commit('stopTokenCheck')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
