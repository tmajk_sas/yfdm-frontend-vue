// STATE
const state = {
  crumb: []
}

// GETTERS
const getters = {
  crumb (state) {
    return state.crumb
  },
  crumbLen (state) {
    return state.crumb.length
  }
}

// MUTATIONS
const mutations = {
  setCrumb (state, payload) {
    state.crumb = payload
  }
}

// ACTIONS
const actions = {
  setCrumb ({ commit, state }, payload) {
    commit('setCrumb', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
