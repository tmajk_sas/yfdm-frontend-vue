import router from '../../router/index'
import { findIndexById } from '../../helpers/array_helpers'
import { getEnvironmentList } from '../../api/test_data'

// STATE
const state = {
  environmentsLoaded: false,
  environments: [],
  activeEnvironment: null,
  activeAddAction: null,
  testDataScreenError: ''
}

// GETTERS
const getters = {
  environmentsLoaded (state) {
    return state.environmentsLoaded
  },
  environments (state) {
    return state.environments
  },
  activeEnvironment (state) {
    return state.activeEnvironment
  },
  activeEnvironmentId (state) {
    return state.activeEnvironment ? state.activeEnvironment.id : null
  },
  activeEnvironmentShortcode (state) {
    return state.activeEnvironment ? state.activeEnvironment.shortcode : null
  },
  activeAddAction (state) {
    return state.activeAddAction
  }
}

// MUTATIONS
const mutations = {
  setEnvironmentsLoaded (state, payload) {
    state.environmentsLoaded = payload
  },
  setActiveEnvironment (state, payload) {
    state.activeEnvironment = payload
  },
  setActiveEnvironmentByShortcode (state, payload) {
    const env = state.environments.find(env => env.shortcode === payload)
    if (env) {
      state.activeEnvironment = env
    } else {
      router.push({name: '404'})
    }
  },
  setEnvironments (state, payload) {
    state.environments = payload
  },
  unshiftEnvironment (state, payload) {
    state.environments.unshift(payload)
  },
  updateEnvironment (state, payload) {
    state.environments.splice(findIndexById(state.environments, payload.id), 1, payload)
  },
  setActiveEnvShortcode (state, payload) {
    state.activeEnvironmentShortcode = payload
  },
  activateEnv (state, payload) {
    state.activeEnvironment = payload
    router.push({
      path: `/environments/${state.activeEnvironment.shortcode}`
    })
  },
  resetEnv (state) {
    state.activeEnvironmentId = null
    state.activeEnvironmentShortcode = null
  },
  setTestDataScreenError (state, payload) {
    state.testDataScreenError = payload
  },
  setActiveAddAction (state, payload) {
    state.activeAddAction = payload
  },
  resetActiveAddAction (state, payload) {
    state.activeAddAction = null
  }
}

// ACTIONS
const actions = {
  setEnvironmentsLoaded ({ commit, state }, payload) {
    commit('setEnvironmentsLoaded', payload)
  },
  setActiveEnvironment ({ commit, state }, payload) {
    commit('setActiveEnvironment', payload)
  },
  setActiveEnvironmentByShortcode ({ commit, state }, payload) {
    commit('setActiveEnvironmentByShortcode', payload)
  },
  setActiveAddAction ({ commit, state }, payload) {
    commit('setActiveAddAction', payload)
  },
  resetActiveAddAction ({ commit, state }) {
    commit('resetActiveAddAction')
  },
  setEnvironments ({ commit, state }, payload) {
    commit('setEnvironments', payload)
  },
  unshiftEnvironment ({ commit, state }, payload) {
    commit('unshiftEnvironment', payload)
  },
  updateEnvironment ({ commit, state }, payload) {
    commit('updateEnvironment', payload)
  },
  activateEnv ({ commit, state }, payload) {
    commit('activateEnv', payload)
  },
  resetEnv ({ commit, state }) {
    commit('resetEnv')
  },
  setTestDataScreenError ({ commit, state }, payload) {
    commit('setTestDataScreenError', payload)
  },
  setActiveEnvShortcode ({ commit, state }, payload) {
    commit('setActiveEnvShortcode', payload)
  },
  getEnvironments ({ commit, state, dispatch }) {
    dispatch('setEnvironmentsLoaded', false)
    dispatch('startGlobalLoading', 'environments')
    getEnvironmentList()
      .then(response => {
        if (!response.error) {
          setTimeout(function () {
            dispatch('setEnvironments', response)
            dispatch('setEnvironmentsLoaded', true)
            if (state.activeEnvironmentShortcode) {
              const env = response.find(env => env.shortcode === state.activeEnvironmentShortcode)
              if (env) {
                dispatch('setActiveEnvironment', env)
              } else {
                dispatch('setActiveEnvironment', env)
                router.push({name: '404'})
              }
            }
            dispatch('setEnvironmentsLoaded', true)
            commit('stopGlobalLoading', 'environments')
          }, 500)
        } else {
          dispatch('setEnvironmentsLoaded', true)
          commit('stopGlobalLoading', 'environments')
          dispatch('setTestDataScreenError', response.error)
          console.log('error geting environments', response.error)
        }
      })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
