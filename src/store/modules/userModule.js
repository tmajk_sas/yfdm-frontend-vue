/* eslint-disable */
// refactor later
import { getHeaders, HTTP, setAccessToken } from '../../http'
import router from '../../router/index'
import { getAuthUserData, loginAndGetToken, apiTokenCheck } from '../../api/login'
const state = {
  user: undefined,
  chekingToken: false,
  globalLoading: false,
  loginErrorMessage: '',
  loginFormLoading: false
}

const getters = {
  user (state) {
    return state.user
  },
  userIsLoggedIn (state) {
    return state.user !== undefined
  },
  loginErrorMessage (state) {
    return state.loginErrorMessage
  },
  loginFormLoading (state) {
    return state.loginFormLoading
  }
}

const mutations = {
  setUser (state, payload) {
    state.user = payload
  },
  setErrorMessage (state, payload) {
    state.loginErrorMessage = payload
  },
  goToLoginScreen (state) {
    window.localStorage.removeItem('yfdmAuthToken')
    window.localStorage.removeItem('yfdmUser')
    state.user = undefined
    router.push({name: 'Login'})
  },
  startLoginFormLoading (state) {
    state.loginFormLoading = true
  },
  stopLoginFormLoading (state) {
    state.loginFormLoading = false
  }
}

const actions = {
  setUser ({ commit, state }, userObj) {
    commit('setUser', userObj)
  },
  logoutUser ({ commit, state }) {
    console.log('logout')
    HTTP.get('logout/', getHeaders())
      .then(response => {
        console.log(response)
      })
    state.user = undefined
  },
  setErrorMessage ({ commit, state }, payload) {
    commit('setErrorMessage', payload)
  },
  goToLoginScreen ({ commit, state }, payload) {
    commit('setErrorMessage', payload)
    commit('goToLoginScreen')
  },
  getUserData ({ commit, state }) {
    getAuthUserData()
      .then(response => {
        if (response.hasOwnProperty('error')) {
          commit('stopLoginFormLoading')
          commit('setErrorMessage', response.error.message)
        } else {
          window.localStorage.setItem('yfdmUser', JSON.stringify(response))
          setTimeout(function () {
            commit('setUser', response)
            commit('stopLoginFormLoading')
            router.push({name: 'Home'})
          }, 500)
        }
      })
  },
  loginUser ({commit, state, dispatch}, payload) {
    commit('startLoginFormLoading')
    loginAndGetToken(payload)
      .then(response => {
        console.log(response)
        if (response.error) {
          commit('stopLoginFormLoading')
          commit('setErrorMessage', response.error.message)
        } else {
          window.localStorage.setItem('yfdmAuthToken', JSON.stringify(response))
          setTimeout(function () {
            dispatch('getUserData')
          }, 500)
        }
      })
  },
  tokenCheck ({commit, state, dispatch}) {
    // this.startTokenCheck()
    commit('startTokenCheck')
    const token = window.localStorage.getItem('yfdmAuthToken')
    console.log('token', token)
    if (token !== null && token !== undefined) {
      apiTokenCheck(JSON.parse(token).refresh)
        .then(response => {
          if (!response.error) {
            setTimeout(function () {
              setAccessToken(response.access)
              commit('stopTokenCheck')
            }, 1000)
          } else {
            commit('stopTokenCheck')
            dispatch('goToLoginScreen', 'You must log in again, session ended')
            // this.goToLoginScreen('You must log in again, session ended')
          }
        })
    } else {
      console.log('no token, redirecting to login screen')
      commit('stopTokenCheck')
      dispatch('goToLoginScreen', 'You must log in')
      // this.stopTokenCheck()
      // this.goToLoginScreen('You must log in')
    }
  },
  localUserCheck ({commit, state, dispatch}) {
    const user = window.localStorage.getItem('yfdmUser')
    if (user !== null && user !== undefined) {
      try {
        commit('setUser', (JSON.parse(user)))
        dispatch('tokenCheck')
      } catch (error) {
        console.log('catched user parse error:', error)
        dispatch('goToLoginScreen', 'You must log in')
      }
    } else {
      console.log('no user, redirect to login')
      dispatch('goToLoginScreen', 'You must log in')
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
