import router from '../../router/index'

// STATE
const state = {
  appsLoaded: false,
  applications: [],
  activeApp: null
}

// GETTERS
const getters = {
  applications (state) {
    return state.applications
  },
  activeApp (state) {
    return state.activeApp
  },
  appsLoaded (state) {
    return state.appsLoaded
  }
}

// MUTATIONS
const mutations = {
  setApplications (state, payload) {
    state.applications = payload
  },
  unshiftApplication (state, payload) {
    state.applications.unshift(payload)
  },
  setActiveApp (state, payload) {
    state.activeApp = payload
  },
  setActiveAppByNameAndEnvId (state, payload) {
    console.log('sarching for app', payload)
    const app = state.applications.find(app => app.name === payload.appName && app.environment_id === payload.envId)
    if (app) {
      state.activeApp = app
    } else {
      router.push({name: '404'})
    }
  },
  setAppsLoaded (state, payload) {
    state.appsLoaded = payload
  }
}

// ACTIONS
const actions = {
  setApplications ({ commit, state }, payload) {
    commit('setApplications', payload)
  },
  unshiftApplication ({ commit, state }, payload) {
    commit('unshiftApplication', payload)
  },
  setActiveApp ({ commit, state }, payload) {
    commit('setActiveApp', payload)
  },
  setActiveAppByNameAndEnvId ({ commit, state }, payload) {
    commit('setActiveAppByNameAndEnvId', payload)
  },
  setAppsLoaded ({ commit, state }, payload) {
    commit('setAppsLoaded', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
