import router from '../../router/index'

// STATE
const state = {
  testDataClasses: [],
  testDataClassesLoaded: false,
  activeTestDataClass: null
}

// GETTERS
const getters = {
  testDataClasses (state) {
    return state.testDataClasses
  },
  testDataClassesLoaded (state) {
    return state.testDataClassesLoaded
  },
  activeTestDataClass (state) {
    return state.activeTestDataClass
  }
}

// MUTATIONS
const mutations = {
  setTestDataClasses (state, payload) {
    state.testDataClasses = payload
  },
  unshiftTestDataClasses (state, payload) {
    state.testDataClasses.unshift(payload)
  },
  setTestDataClassesLoaded (state, payload) {
    state.testDataClassesLoaded = payload
  },
  setActiveTestDataClass (state, payload) {
    state.activeTestDataClass = payload
  },
  setActiveTdcByActiveAppIdAndUrlAppName (state, payload) {
    const atdc = state.testDataClasses.find(
      tdc => tdc.app_id === payload.appId &&
      tdc.name.toLowerCase() === payload.tdcName.toLowerCase()
    )
    if (atdc) {
      state.activeTestDataClass = atdc
    } else {
      console.log('active tdc not found during mutation')
      router.push({name: '404'})
    }
  }
}

// ACTIONS
const actions = {
  setTestDataClasses ({ commit, state }, payload) {
    commit('setTestDataClasses', payload)
  },
  unshiftTestDataClasses ({ commit, state }, payload) {
    commit('unshiftTestDataClasses', payload)
  },
  setTestDataClassesLoaded ({ commit, state }, payload) {
    commit('setTestDataClassesLoaded', payload)
  },
  setActiveTestDataClass ({ commit, state }, payload) {
    commit('setActiveTestDataClass', payload)
  },
  setActiveTdcByActiveAppIdAndUrlAppName ({ commit, state }, payload) {
    commit('setActiveTdcByActiveAppIdAndUrlAppName', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
