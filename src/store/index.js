import Vuex from 'vuex'
import Vue from 'vue'
import userModule from './modules/userModule'
import appGlobal from './modules/appGlobal'
import environments from './modules/environments'
import applications from './modules/applications'
import testDataClasses from './modules/testDataClasses'
import crumb from './modules/crumb'
Vue.use(Vuex)
export const store = new Vuex.Store({
  modules: {
    crumb,
    userModule,
    appGlobal,
    environments,
    applications,
    testDataClasses
  }
})
