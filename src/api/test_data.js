import { handleResponse, handleError } from './api_response_resolver'
import { HTTP, getHeaders } from '../http'

let HTTP_GET = HTTP.get
let HTTP_POST = HTTP.post

export const getEnvironmentList = (httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  const url = 'test-data/list-environments/'
  return HTTP_GET(url, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}

export const getEnvironmentApps = (envShortCode, httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  const url = `test-data/list-environment-apps/?env_shortcode=${envShortCode}`
  return HTTP_GET(url, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}

export const getAppTdcs = (envShortCode, appName, httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  const url = `test-data/list-app-test-data-classes/?env_shortcode=${envShortCode}&app_name=${appName}`
  return HTTP_GET(url, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}

export const getDataRows = (envShortCode, appName, tdcName, httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  const url = `test-data/list-data-rows/?env_shortcode=${envShortCode}&app_name=${appName}&tdcname=${tdcName}`
  return HTTP_GET(url, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}

export const addNewEnvironment = (postData, httpPost) => {
  if (httpPost) {
    HTTP_POST = httpPost
  }
  const url = 'test-data/add-new-environment/'
  return HTTP_POST(url, postData, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}
