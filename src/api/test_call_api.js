import { handleNonSuccessResponse, handleError } from './api_response_resolver'
import { HTTP } from '../http'

let HTTP_GET = HTTP.get

export const getTestApiHelloData = (userName, httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  let url = 'test/' + userName + '/'
  return HTTP_GET(url)
    .then(response => {
      if (response.status !== 200) {
        return handleNonSuccessResponse(response)
      } else {
        response.data.username = userName
        return response.data
      }
    })
    .catch(error => {
      return handleError(error)
    })
}
