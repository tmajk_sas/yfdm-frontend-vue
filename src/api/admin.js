import { handleResponse, handleError } from './api_response_resolver'
import { HTTP, getHeaders } from '../http'

// let HTTP_POST = HTTP.post
let HTTP_GET = HTTP.get
let HTTP_POST = HTTP.post

export const getAdminUserList = (httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  const url = 'admin/user-list/'
  return HTTP_GET(url, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}

export const addNewUser = (postData, httpPost) => {
  if (httpPost) {
    HTTP_POST = httpPost
  }
  const url = 'admin/add-user/'
  return HTTP_POST(url, postData, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}
