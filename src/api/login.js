import { handleResponse, handleError } from './api_response_resolver'
import { HTTP, getHeaders } from '../http'
import { getErrorObject } from './errors'

let HTTP_POST = HTTP.post
let HTTP_GET = HTTP.get

export const loginAndGetToken = (postData, httpPost) => {
  if (httpPost) {
    HTTP_POST = httpPost
  }
  console.log(HTTP_POST)
  const url = 'token/'
  return HTTP_POST(url, postData)
    .then(response => {
      if (response.status === 400) {
        let error = getErrorObject(123)
        console.log('loginAndGetToken 400 error:', error)
        return Promise.resolve({error})
      } else {
        return handleResponse(response)
      }
    })
    .catch(error => {
      return handleError(error)
    })
}

export const getAuthUserData = (httpGet) => {
  if (httpGet) {
    HTTP_GET = httpGet
  }
  const url = 'get-user-data/'
  return HTTP_GET(url, getHeaders())
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}

export const apiTokenCheck = (token, httpPost) => {
  if (httpPost) {
    HTTP_POST = httpPost
  }
  const url = 'token/refresh/'
  const tokenObj = {'refresh': token}
  return HTTP_POST(url, tokenObj)
    .then(response => handleResponse(response))
    .catch(error => handleError(error))
}
