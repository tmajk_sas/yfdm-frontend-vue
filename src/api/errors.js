// error.status must be unique
const errors = [
  {
    status: 888,
    error: 'OTHER_ERROR',
    message: 'Unexpeced/unnown server error occured'
  },
  {
    status: 123,
    error: 'AUTH_ERROR',
    message: 'Wrong login or password'
  },
  {
    status: 500,
    error: '500+_ERROR',
    message: 'Backend Server error'
  },
  {
    status: 999,
    error: 'CONN_TIMEOUT',
    message: 'Connection timeout, try again'
  },
  {
    status: 666,
    error: 'NETWORK_ERROR',
    message: 'Network error, backend API unreachable'
  },
  {
    status: 404,
    error: 'NOT_FOUND',
    message: '404 Resource not found'
  },
  {
    status: 400,
    error: 'BAD_REQUEST',
    message: 'Bad request'
  },
  {
    status: 403,
    error: 'FORBIDDEN',
    message: 'You don\'t have access to that resource, please contact app administrator'
  },
  {
    status: 401,
    error: 'UNAUTHORIZED',
    message: 'You are not authorized(JWT is missing in request headers)'
  }
]
export const getErrorObject = (status) => {
  let error = errors.find(error => error.status === status)
  return error !== undefined ? error : errors[0] // errors[0] is other error
}
