import { getErrorObject } from './errors'
import router from '../router/index'

export const handleResponse = (response) => {
  console.log('response: ', response)
  if (response.status !== 200) {
    return handleNonSuccessResponse(response)
  } else {
    return Promise.resolve(response.data)
  }
}

export const handleNonSuccessResponse = (response) => {
  let error = getErrorObject(response.status)
  let strError = JSON.stringify(error)
  console.log(`handleNonSuccessResponse: ${response.status}:, ${strError}`)
  if (error.status === 403) {
    router.push({name: 'Denied'})
  }
  if (response.data !== null && response.data !== undefined) {
    if (response.data.hasOwnProperty('errors')) {
      error.errors = response.data.errors
      console.log('got errors from api:', error)
    }
  }
  return Promise.resolve({error})
}

// handle errors
export const handleError = (errorResponse) => {
  console.log('error!!!', errorResponse)
  // network error
  if (!errorResponse.response && !errorResponse.code) {
    let error = getErrorObject(666)
    console.log('handleErrorResponse:', error)
    return Promise.resolve({error})
  } else {
    // timeout
    if (errorResponse.code === 'ECONNABORTED') {
      let error = getErrorObject(999)
      console.log('handleErrorResponse:', error)
      return Promise.resolve({error})
      // 500+ z serwera
    } else if (errorResponse.response.status >= 500) {
      let error = getErrorObject(500)
      error.message = `${error.message} : "${errorResponse.response.status} - ${errorResponse.response.statusText}"`
      console.log('handleErrorResponse:', error)
      return Promise.resolve({error})
    }
  }
}
