import { token, adminUserResponse } from './resource/login_resource'
import { userList, newUserFromPostData, newEnvFromPostData } from './resource/admin_resource'
import { environmentsList, environmentApps, testDataClasses, dataRows } from '../dev_mock/resource/test_data_resource'

const apiData = [
  {url: 'logout/', data: null},
  {url: 'token/', data: token},
  {url: 'get-user-data/', data: adminUserResponse},
  {url: 'token/refresh/', data: token},
  {url: 'admin/user-list/', data: userList},
  {url: 'admin/add-user/', data: newUserFromPostData},
  {url: 'test-data/list-environments/', data: environmentsList},
  {url: 'test-data/list-environment-apps/', data: environmentApps},
  {url: 'test-data/list-app-test-data-classes/', data: testDataClasses},
  {url: 'test-data/add-new-environment/', data: newEnvFromPostData},
  {url: 'test-data/list-data-rows/', data: dataRows}
]

const getReponseData = function (url) {
  return {
    status: 200,
    data: apiData.find(response => {
      let endIndex = null
      if (url.indexOf('?') !== -1) {
        endIndex = url.indexOf('?')
      }
      if (endIndex === null) {
        return response.url === url
      }
      return response.url === url.substring(0, endIndex)
    }).data
  }
}

function isFunction (x) {
  return Object.prototype.toString.call(x) === '[object Function]'
}

export const mockHTTP = {
  get: function (url) {
    return Promise.resolve(getReponseData(url))
  },
  post: function (url, data) {
    // based on data logic later
    let response = getReponseData(url)
    if (isFunction(response.data)) {
      response.data = response.data(data)
      return Promise.resolve(response)
    }
    return Promise.resolve(getReponseData(url))
  }
}
