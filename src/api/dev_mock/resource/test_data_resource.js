export const environmentsList = [
  {
    id: 1,
    shortcode: 'bet',
    name: 'beta',
    description: 'Sriracha glossier butcher poke. Tumeric marfa normcore hella scenester jianbing. 8-bit church-key franzen raw denim photo booth helvetica af letterpress tumeric pop-up meditation freegan edison bulb locavore. ',
    added_by_id: 1,
    added_by_username: 'admin',
    added_date: '2017-12-18T09:43:24.848941'
  },
  {
    id: 2,
    shortcode: 'alf',
    name: 'alfa',
    description: 'Umami godard hot chicken fanny pack. Selfies kinfolk literally brunch disrupt post-ironic. Unicorn seitan DIY vaporware leggings vegan PBR&B snackwave. Pitchfork banjo meditation cold-pressed hashtag. ',
    added_by_id: 1,
    added_by_username: 'admin',
    added_date: '2017-12-18T11:22:27.919295'
  }
]

export const environmentApps = [
  {
    'environment_id': 1,
    'environment_shortcode': 'bet',
    'environment_name': 'beta',
    'id': 1,
    'name': 'great app',
    'description': 'Umami godard hot chicken fanny pack. Selfies kinfolk literally brunch disrupt post-ironic. Unicorn seitan DIY vaporware leggings vegan PBR&B snackwave. Pitchfork banjo meditation cold-pressed hashtag. ',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-18T09:43:42.881197'
  },
  {
    'environment_id': 1,
    'environment_shortcode': 'bet',
    'environment_name': 'beta',
    'id': 1,
    'name': 'another-app',
    'description': 'Banjo XOXO la croix, offal hell of single-origin coffee scenester pickled asymmetrical before they sold out. Food truck echo park +1 vape, lumbersexual shabby chic literally edison bulb yr readymade poke pop-up',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-18T09:43:42.881197'
  }
]

export const testDataClasses = [
  {
    'app_id': 1,
    'app_name': 'great app',
    'id': 1,
    'name': 'appUsername',
    'data_type': 'STR',
    'data_type_display': 'String',
    'limited_use': false,
    'use_limit': null
  },
  {
    'app_id': 1,
    'app_name': 'great app',
    'id': 2,
    'name': 'limitedClass',
    'data_type': 'JSO',
    'data_type_display': 'JSON',
    'limited_use': true,
    'use_limit': 1
  }
]

export const dataRows = [
  {
    'id': 2,
    'tdc_id': 1,
    'tdc_name': 'appUsername',
    'row_data': {
      'value': 'john'
    },
    'tdc_type_display': 'String',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-18T12:23:53.004671',
    'locked': false,
    'used': false,
    'status': 'Locked'
  },
  {
    'id': 1,
    'tdc_id': 1,
    'tdc_name': 'appUsername',
    'row_data': {
      'value': 'jimmy'
    },
    'tdc_type_display': 'String',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-22T11:41:16.463024',
    'locked': false,
    'used': true,
    'status': 'Used'
  },
  {
    'id': 3,
    'tdc_id': 1,
    'tdc_name': 'appUsername',
    'row_data': {
      'value': 'john'
    },
    'tdc_type_display': 'String',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-11-18T12:23:53.004671',
    'locked': false,
    'used': false,
    'status': 'Available'
  },
  {
    'id': 4,
    'tdc_id': 1,
    'tdc_name': 'appUsername',
    'row_data': {
      'value': 'jimmy'
    },
    'tdc_type_display': 'String',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-13T11:41:16.463024',
    'locked': false,
    'used': true,
    'status': 'Used'
  },
  {
    'id': 6,
    'tdc_id': 1,
    'tdc_name': 'appUsername',
    'row_data': {
      'value': 'john'
    },
    'tdc_type_display': 'String',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-12T12:23:53.004671',
    'locked': false,
    'used': false,
    'status': 'Available'
  },
  {
    'id': 5,
    'tdc_id': 1,
    'tdc_name': 'appUsername',
    'row_data': {
      'value': 'jimmy'
    },
    'tdc_type_display': 'String',
    'added_by_id': 1,
    'added_by_username': 'admin',
    'added_date': '2017-12-11T11:41:16.463024',
    'locked': false,
    'used': true,
    'status': 'Used'
  }
]
