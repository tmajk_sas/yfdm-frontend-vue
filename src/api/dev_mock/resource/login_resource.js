export const token = {
  refresh: '1234qwer',
  access: '1234qwer'
}

export const adminUserResponse = {
  username: 'admin',
  last_login: '2018-01-26T13:41:21.405674',
  is_superuser: true
}

export const normalUserResponse = {
  username: 'user1',
  last_login: '2018-01-26T13:41:21.405674',
  is_superuser: false
}
