export const userList = [
  {
    id: 1,
    last_login: '2018-01-26T13:41:21.405674',
    is_superuser: true,
    username: 'admin',
    first_name: 'Tomasz',
    last_name: 'Majk',
    email: 'sas.it.tomasz.majk@gmail.com',
    is_staff: true,
    is_active: true,
    date_joined: '2017-12-18T09:41:47',
    groups: [],
    user_permissions: []
  },
  {
    id: 2,
    last_login: '2018-01-24T13:39:18.144357',
    is_superuser: false,
    username: 'user1',
    first_name: 'Jimmy',
    last_name: 'Kovalsky',
    email: 'jimmy@kow.pl',
    is_staff: false,
    is_active: true,
    date_joined: '2018-01-19T07:50:41',
    groups: [],
    user_permissions: []
  }
]

function getRandomInt (max) {
  return Math.floor(Math.random() * Math.floor(max))
}

export const newUserFromPostData = function (postData) {
  postData.id = getRandomInt(99)
  postData.date_joined = new Date().toLocaleString()
  return postData
}
export const newEnvFromPostData = function (postData) {
  postData.id = getRandomInt(99)
  postData.added_date = new Date().toLocaleString()
  postData.added_by_id = getRandomInt(99)
  postData.added_by_username = 'mockAdminUser'
  return postData
}
