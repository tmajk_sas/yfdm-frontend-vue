/* eslint-disable */
export const validators = {
  methods: {
    registerFields (fieldsObj) {
      Object.keys(fieldsObj).forEach(field => {
        this.fields.push(fieldsObj[field])
      })
    },
    getField (fieldId) {
      return this.fields.find(field => field.id === fieldId)
    },
    activateField (fieldId) {
      this.activeFieldId = fieldId
    },
    validateField (fieldId) {
      let field = this.getField(fieldId)
      field.errors = []
      field.rules.forEach(r => {
        let rule = this.validationRules.find(vr => vr.rule === r.name)
        let result = 'validation error within validator rule! debug it!'
        if (r.params !== null) {
          result = rule.validator(field, r.params)
        } else {
          result = rule.validator(field)
        }
        this.validationLog.push({
          field: field,
          validator: rule.validator,
          result: result
        })
        if (result !== true) {
          field.errors.push(result)
          return
        }
      })
    },
    hasBeenValidated (fieldId) {
      let field = this.getField(fieldId)
      if (!field.validate) {
        return true
      }
      return this.validationLog.filter(log => log.field.id === fieldId).length > 0
    },
    _isLastInvalidField () {
      let invalidFields = this.fields.filter(field => {
        if (!field.validate) { return false }
        return field.errors.length > 0
      })
      if (invalidFields.length === 1) {
        return invalidFields[0].id === this.activeFieldId
      }
      return false
    },
    _isLastActiveField () {
      let fieldCount = 0
      let validatedFieldCount = 0
      this.fields.forEach(field => {
        if (field.validate) {
          fieldCount++
          if (this.hasBeenValidated(field.id)) {
            validatedFieldCount++
          }
        }
      })
      return fieldCount === validatedFieldCount + 1
    },
    /* @SECTION */
    /* VALIDATION FUNCTIONS */
    _hasNumber(field) {
      return /\d/.test(field.value) ? true : 'This field must contain at least one number'
    },
    _hasMaxLength (field, params) {
      return field.value.length <= params.length ? true : `This field is supposed to be max ${params.length} chars`
    },
    _hasMinLength (field, params) {
      return field.value.length >= params.length ? true : `This field is supposed to be min ${params.length} chars`
    },
    _passwordRepeatMatches (field) {
      return this.getField('password').value === field.value ? true : 'Passwords must be the same'
    },
    _emailIsValid (field) {
      let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return re.test(field.value.toLowerCase()) ? true : 'Enter a valid email adress. Example: "myadress@domain.com"'
    },
    _notEmpty (field) {
      return field.value !== '' ? true : 'This field is required'
    }
  },
  computed: {
    activeFieldValue () {
      return this.getField(this.activeFieldId).value
    }
  },
  watch: {
    activeFieldVal (val, oldVal) {
      if (this._isLastInvalidField()) {
        this.validateField(this.activeFieldId)
      }
      if (this.hasBeenValidated(this.activeFieldId) || this._isLastActiveField(this.activeFieldId)) {
        this.validateField(this.activeFieldId)
      }
      // if psswd field change validate also psswd repeat
      if (this.activeFieldId === 'password' && this.hasBeenValidated('password_repeat')) {
        this.validateField('password_repeat')
      }
    },
    fields: {
      deep: true,
      handler: function (val, oldVal) {
        this.activeFieldVal = this.activeFieldValue
        let valid = true
        val.forEach(field => {
          if (field.validate) {
            if (field.errors.length > 0 || !this.hasBeenValidated(field.id)) { 
              valid = false
              return
            }
          }
        })
        this.fieldsAreValid = valid
      }
    }
  },
  data () {
    const rules = [
      {rule: 'has_number', validator: this._hasNumber},
      {rule: 'max_length', validator: this._hasMaxLength},
      {rule: 'min_length', validator: this._hasMinLength},
      {rule: 'not_empty', validator: this._notEmpty},
      {rule: 'is_email', validator: this._emailIsValid},
      {rule: 'same_as_password', validator: this._passwordRepeatMatches},
    ]
    return {
      fields: [],
      validationRules: rules,
      validationLog: [],
      activeFieldId: '',
      fieldsAreValid: false,
      activeFieldVal: ''
    }
  }
}
