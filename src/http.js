import axios from 'axios'
import { baseApiUrl, mockApi } from './config'
import { mockHTTP } from './api/dev_mock/mock_http'

const getHTTP = function () {
  console.log('mock?', mockApi)
  var http
  if (mockApi) {
    http = mockHTTP
    console.log(http)
  } else {
    http = axios.create({
      baseURL: baseApiUrl,
      timeout: 10000,
      responseType: 'json',
      maxContentLength: 2000000,
      validateStatus: function (status) {
        return status >= 200 && status < 500 // default
      }
    })
  }
  console.log('http:', http)
  return http
}

export const HTTP = getHTTP()

export const getAccessToken = function () {
  let token = window.localStorage.getItem('yfdmAuthToken')
  if (token !== undefined) {
    token = JSON.parse(window.localStorage.getItem('yfdmAuthToken')).access
  }
  return token
}

export const setAccessToken = function (newAccessToken) {
  let token = JSON.parse(window.localStorage.getItem('yfdmAuthToken'))
  token.access = newAccessToken
  window.localStorage.setItem('yfdmAuthToken', JSON.stringify(token))
}

export const getHeaders = function () {
  return {
    headers: {
      // 'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + getAccessToken()
    }
  }
}
