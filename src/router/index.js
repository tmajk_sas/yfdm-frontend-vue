import Vue from 'vue'
import Router from 'vue-router'
import Denied from '@/components/Denied'
import NotFound from '@/components/NotFound'
import LoginScreen from '@/components/login_screen/LoginScreen'
import HomeScreen from '@/components/home_screen/HomeScreen'
import AdminScreen from '@/components/admin_screen/AdminScreen'
import DocsScreen from '@/components/docs_screen/DocsScreen'
import TestDataScreen from '@/components/test_data_screen/TestDataScreen'
import TestDataScreenEnvironDetails from '@/components/test_data_screen/TestDataScreenEnvironDetails'
import TestDataScreenAppDetails from '@/components/test_data_screen/TestDataScreenAppDetails'
import TestDataScreenTestDataClassDetails from '@/components/test_data_screen/TestDataScreenTestDataClassDetails'
import ToolsScreen from '@/components/tools_screen/ToolsScreen'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/tools',
      name: 'Tools',
      component: ToolsScreen
    },
    {
      path: '/environments/',
      name: 'TestData',
      component: TestDataScreen,
      children: [
        {
          path: ':shortcode',
          component: TestDataScreenEnvironDetails,
          children: [
            {
              path: ':appname',
              component: TestDataScreenAppDetails,
              children: [
                {
                  path: ':tdcname',
                  component: TestDataScreenTestDataClassDetails
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: '/docs',
      name: 'Docs',
      component: DocsScreen
    },
    {
      path: '/404',
      alias: '*',
      name: '404',
      component: NotFound
    },
    {
      path: '/denied',
      name: 'Denied',
      component: Denied
    },
    {
      path: '/admin',
      name: 'Administration',
      component: AdminScreen
    },
    {
      path: '/',
      name: 'Home',
      component: HomeScreen
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginScreen
    }
  ]
})
