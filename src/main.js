import Vue from 'vue'
import App from './App'
import router from './router'
import Buefy from 'buefy'
import { store } from './store/index'
import jQuery from 'jquery'
global.$ = jQuery

Vue.config.productionTip = false
Vue.use(Buefy)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
