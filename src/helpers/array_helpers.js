export const findIndexById = function (array, objectId) {
  return array.findIndex(el => el.id === objectId)
}
